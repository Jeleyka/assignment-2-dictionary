%include "lib.inc"
%include "words.inc"
%include "dict.inc"
%define MAX_BUFFER_SIZE 256

global _start
section .bss
buffer: resb MAX_BUFFER_SIZE

section .rodata
key_err: db "No has element with this key", 10, 0
size_err: db "Text is too long", 10, 0

section .text

global _start

exit_w_err:
    mov rsi, 2
    call print_string
    mov rdi, 1
    jmp exit

_start:
    mov rdi, buffer
    mov rsi, MAX_BUFFER_SIZE
    call read_word
    test rax, rax
    jz .size
    mov rdi, rax
    mov rsi, nxt_el
    call find_word
    test rax, rax
    jz .key
    mov rdi, rax
    push rdi
    add rdi, 8
    call string_length
    pop rdi
    add rdi, rax
    add rdi, 9
	mov rsi, 1
    call print_string
    call print_newline
    xor rdi, rdi
    jmp exit
    .size:
        mov rdi, size_err
        jmp exit_w_err
    .key:
        mov rdi, key_err
        jmp exit_w_err

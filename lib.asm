section .text
 

global exit, string_length, print_string, print_char, print_newline, print_uint, print_int, string_equals, read_char, read_word, parse_uint, parse_int, string_copy

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
 
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .step:
        cmp byte[rax + rdi], 0
        je .end
        inc rax
        jmp .step
    .end:
        ret
 
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push    rdi
	push 	r8
    call    string_length
	pop		r8
    pop     rdi
	mov 	r8, rsi
    mov     rdx, rax
    mov     rax, 1
    mov     rsi, rdi
    mov     rdi, r8
    syscall
    ret
 
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov     rax, 1
    mov     rsi, rsp
    mov     rdi, 1
    mov     rdx, 1
    syscall
    pop rdi
    ret
 
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    call print_char
    ret
 
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    push 0
    sub rsp, 20
    .step:
	xor rdx, rdx
	div r8
	add rdx, '0'
	dec rdi
	mov [rdi], dl
	cmp rax, 0
	jnz .step
    call print_string
    add rsp, 28
    ret
 
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .uint:
	jmp print_uint
 
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .step:
	mov dh, [rdi + rax]
        cmp dh, [rsi + rax]
        jne .false
        cmp dh, 0
        je .true
        inc rax
        jmp .step
    .false:
        xor rax, rax
	ret
    .true:
        mov rax, 1
    	ret
 
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor     rax, rax
    xor     rdi, rdi
    mov     rsi, rsp
    mov     rdx, 1
    syscall
    pop rax
    ret
 
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
 
read_word: ; rdi, rsi
    xor rax, rax
    xor rdx, rdx
    .skip_whitespaces:
    push rdi
    push rsi
    push rdx
	call read_char
    pop rdx
    pop rsi
    pop rdi
	cmp rax, ' '
	je .skip_whitespaces
	cmp rax, `\n`
	je .skip_whitespaces
	cmp rax, `\t`
	je .skip_whitespaces
    .step:
	mov [rdi + rdx], rax
	cmp rax, 0
	je .end
	cmp rax, ' '
	je .end
	cmp rax, `\n`
	je .end
	cmp rax, `\t`
	je .end
	inc rdx
	cmp rdx, rsi
	je .err
	push rdi
    push rsi
    push rdx
	call read_char
    pop rdx
    pop rsi
    pop rdi
	jmp .step
    .end:
	mov byte[rdi+rdx], 0
	inc rdx
	mov rax, rdi
	ret
    .err:
	xor rax, rax
	ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r8, r8
    xor rdx, rdx
    xor rax, rax
    .step:
	mov r8b, [rdi + rdx]
	cmp r8b, '0'
	jb .end
	cmp r8b, '9'
	ja .end
	sub r8b, '0'
	imul rax, 10
	add rax, r8
	inc rdx
	jmp .step
    .end:
	ret
 
 
 
 
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret 
 
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: ; rdi, rsi, rdx
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    inc rax
    cmp rax, rdx
    jg .err
    xor rax, rax
    .step:
	mov rcx, [rdi + rax]
	mov [rsi + rax], rcx
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .step
    .end:
        ret
    .err:
	xor rax, rax
	ret

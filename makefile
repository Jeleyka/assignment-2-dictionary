ASM=nasm
ASMFLAGS=-felf64
LD=ld
RM=rm

.PHONY:
	clean

clean:
	$(RM) *.o

main: main.o lib.o dict.o
	$(LD) -o main $^
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
